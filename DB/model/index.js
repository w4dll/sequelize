// //如统一引入后规定外键
// const sql = require('../db')
// const Sequelize = require('sequelize');
// const moment = require('moment');
// const { NULL } = require('mysql/lib/protocol/constants/types');

// /*
// 数据类型
// Sequelize.STRING // VARCHAR(255)
// Sequelize.STRING(1234) // VARCHAR(1234)
// Sequelize.STRING.BINARY // VARCHAR BINARY
// Sequelize.TEXT // TEXT
// Sequelize.TEXT('tiny') // TINYTEXT

// Sequelize.INTEGER // INTEGER
// Sequelize.BIGINT // BIGINT
// Sequelize.BIGINT(11) // BIGINT(11)

// Sequelize.FLOAT // FLOAT
// Sequelize.FLOAT(11) // FLOAT(11)
// Sequelize.FLOAT(11, 12) // FLOAT(11,12)


// Sequelize.DOUBLE // DOUBLE
// Sequelize.DOUBLE(11) // DOUBLE(11)
// Sequelize.DOUBLE(11, 12) // DOUBLE(11,12)

// Sequelize.DECIMAL // DECIMAL
// Sequelize.DECIMAL(10, 2) // DECIMAL(10,2)

// Sequelize.DATE // DATETIME 针对 mysql / sqlite, TIMESTAMP WITH TIME ZONE 针对 postgres
// Sequelize.DATE(6) // DATETIME(6) 针对 mysql 5.6.4+. 小数秒支持多达6位精度
// Sequelize.DATEONLY // DATE 不带时间.
// Sequelize.BOOLEAN // TINYINT(1)

// Sequelize.ENUM('value 1', 'value 2') // 一个允许具有 “value 1” 和 “value 2” 的 ENUM


// Sequelize.GEOMETRY // 空间列. 仅限于 PostgreSQL (具有 PostGIS) 或 MySQL.
// Sequelize.GEOMETRY('POINT') // 具有几何类型的空间列. 仅限于 PostgreSQL (具有 PostGIS) 或 MySQL.
// Sequelize.GEOMETRY('POINT', 4326) // 具有几何类型和SRID的空间列. 仅限于 PostgreSQL (具有 PostGIS) 或 MySQL.
// */

// const Goods = sql.define('tb_item',{
//     id:{
//         type: Sequelize.INTEGER,
//         autoIncrement: true,
//         primaryKey: true,
//         allowNull: false
//     },
//     title:{
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     sell_point:{
//         type: Sequelize.TEXT,
//         allowNull: true,
//         defaultValue: null
//     },
//     price:{
//         type: Sequelize.BIGINT(20),
//         allowNull: false
//     },
//     num:{
//         type: Sequelize.INTEGER,
//         allowNull: false
//     },
//     barcode:{
//         type: Sequelize.STRING(30),
//         allowNull: true,
//         defaultValue: null
//     },
//     image:{
//         type: Sequelize.STRING(500),
//         allowNull: true,
//         defaultValue: null
//     },
//     cid:{
//         type: Sequelize.BIGINT(10),
//         allowNull: false,
//     },
//     status:{
//         type: Sequelize.INTEGER,
//         allowNull: false,
//     },
//     created:{
//         type: Sequelize.DATE,
//         allowNull: false,
//         get(){
//             return moment(this.getDataValue('created')).format('YYYY-MM-DD HH:mm:ss')
//         }
//     },
//     updated:{
//         type: Sequelize.DATE,
//         allowNull: false,
//         get(){
//             return moment(this.getDataValue('updated')).format('YYYY-MM-DD HH:mm:ss')
//         }
//     }
// },{
//     // 注意需要加上这个， egg-sequelize只是简单的使用Object.assign对配置和默认配置做了merge, 
//         // 如果不加这个 update_at会被转变成 updateAt故报错
//         underscored: false,
//         // 取消自动维护时间戳 [ created_at、updated_at ]
//         timestamps: false,
//         // 禁止修改表名，默认情况下，sequelize将自动将所有传递的模型名称（define的第一个参数）转换为复数
//         // 但是为了安全着想，复数的转换可能会发生变化，所以禁止该行为
//         freezeTableName: true,
//         tableName: `tb_item`,
// })

// const User = sql.define('user',{
//     id:{
//         type: Sequelize.INTEGER,
//         autoIncrement: true,
//         primaryKey: true,
//         allowNull: false
//     },
//     name:{
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     u_num:{
//         type: Sequelize.INTEGER(10),
//         allowNull: false,
//         defaultValue:NULL
//     },
//     password:{
//         type: Sequelize.STRING,
//         allowNull: false,
//         defaultValue:NULL
//     },
//     connect_type:{
//         type: Sequelize.INTEGER,
//         allowNull: false,
//         defaultValue:0
//     }
// },{
//     // 注意需要加上这个， egg-sequelize只是简单的使用Object.assign对配置和默认配置做了merge, 
//         // 如果不加这个 update_at会被转变成 updateAt故报错
//         // underscored: false,
//         // 取消自动维护时间戳 [ created_at、updated_at ]
//         // timestamps: false,
//         // 禁止修改表名，默认情况下，sequelize将自动将所有传递的模型名称（define的第一个参数）转换为复数
//         // 但是为了安全着想，复数的转换可能会发生变化，所以禁止该行为
//         freezeTableName: true,
//         // tableName: `user`,
// })

// module.exports = {
//     Goods,
//     User
// }
const controllers = require('require-all')({
    dirname: __dirname + '/model',
  })
  
  console.log('model==========================>',controllers)

///如有老数据并且 时间自己维护的需要加配置
   // 注意需要加上这个， egg-sequelize只是简单的使用Object.assign对配置和默认配置做了merge, 
//         // 如果不加这个 update_at会被转变成 updateAt故报错
//         underscored: false,
//         // 取消自动维护时间戳 [ created_at、updated_at ]
//         timestamps: false,
//         // 禁止修改表名，默认情况下，sequelize将自动将所有传递的模型名称（define的第一个参数）转换为复数
//         // 但是为了安全着想，复数的转换可能会发生变化，所以禁止该行为
//         freezeTableName: true,
//         tableName: `tb_item`,

  module.exports = {
      ...controllers
  }
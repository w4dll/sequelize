const { Sequelize } = require('sequelize');
const sqlConfig = require('../config/dbConfig');

const sql = new Sequelize(sqlConfig.config.database, sqlConfig.config.username, sqlConfig.config.pwd, sqlConfig.config);//创建数据库链接

//测试数据库连接是否成功
    sql.authenticate().then(()=>{
        console.log('数据库连接成功');
    }).catch((err)=>{
        console.log('数据库连接失败',err)
    })

    // 创建同步表   force = true  时会把存在的表先 drop 掉再创建，好怕怕 alter = true 同步字段
    sql.sync({
        force: false,
        alter: true
    }).catch(
        err =>{
            console.log(err)
        }
    )

module.exports = sql
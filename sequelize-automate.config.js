const sqlConfig = require('./config/dbConfig');
//配置根据数据库生成model
module.exports = {
  dbOptions: {
    ...sqlConfig.config,
    logging: false
  },
  options: {
    type: "js",
    dir: "DB/model"
 }
}
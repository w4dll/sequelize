const jsToken = require('jsonwebtoken')
const { tConfig } = require('../config/tokenConfig')
class util{
        //get请求参数校验
        get_check_data(check=[],req,res){
            const checkData  = check //自定义校验接口必传参数  type:Array
            let returnData = {
                pass:true,
                msg:''
            }
            if(checkData.length!=0){
                let str = '缺少'
                for (let index = 0; index < checkData.length; index++) {
                    if(!req.query[checkData[index]]){
                        str += `${ checkData[index] }${ index!=checkData.length-1?'、':'' }`
                    }
                }
                if(str!=='缺少'){
                    returnData.pass = false;
                    returnData.msg = str
                }
            }
            if(!returnData.pass) {
                return res.send({
                    code:1,
                    msg:returnData.msg
                },500)
            }else{
                return returnData
            }
             
        }
        //post请求参数校验
         post_check_data(check=[],req,res){
             console.log('req',req.body);
            const checkData  = check //自定义校验接口必传参数 type:Array
            let returnData = {
                pass:true,
                msg:''
            }
            if(checkData.length!=0){
                let str = '缺少'
                for (let index = 0; index < checkData.length; index++) {
                    if(!req.body[checkData[index]]){
                        str += `${ checkData[index] }${ index!=checkData.length-1?'、':'' }`
                    }
                }
                if(str!=='缺少'){
                    returnData.pass = false;
                    returnData.msg = str
                }
            }

            if(!returnData.pass) {
                return res.send({
                    code:1,
                    msg:returnData.msg
                },500)
            }else{
                return returnData
            }
        }

        //时间戳转换
         rTime(date) { var json_date = new Date(date).toJSON();
            return new Date(+new Date(json_date) + 8 * 3600 * 1000).toISOString().replace(/T/g, ' ').replace(/\.[\d]{3}Z/, '') 
        }

        async CreatToken(uid){
            const Key = tConfig.key
            const prescription = tConfig.expiresIn
            const token = jsToken.sign({uid},Key,{expiresIn:prescription})
            return `Bearer ${token}`
        }
}




module.exports =  new util()
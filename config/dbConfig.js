module.exports = {
    config:{
        host : 'localhost',
        port : '3306', 
        dialect : 'mysql',
        
        pool:{
            max: 10,             // 连接池最大连接数量
            min: 0,             // 连接池最小连接数量
            idle: 10000,         // 如果一个线程超过10秒钟没有被使用过就释放该线程
            acquire: 30000,        // 多久没有获取到连接就断开
        },
        
        database:'taotao',            //数据库名称
        username:'root',        
        pwd:'root',
        timezone: "+08:00"
    }
}
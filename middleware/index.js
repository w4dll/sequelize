const express = require('express');
const jwt = require('jsonwebtoken');
const { tConfig } = require('../config/tokenConfig') 
class middleWare{
    //处理404
    not_found_middle(req,res,next){
        // res.status = 404
        console.log('err');
        // res.send({msg:'接口不存在'},404)
        // throw new Error('404')

        // next()
        const err = new Error('Not Found');  
        err.status = 404;
        err.message = '接口不存在';
        next(err);
        
        // throw err
    }

    //参数缺失报错
    // lack_parameter(err,req,res,next){
    //     if(err){
    //         res.status = 500
    //         res.send({msg:'系统发生错误，请联系管理员'},500)
    //     }
    // }
    
}

module.exports = new middleWare()
var express = require('express');
var router = express.Router();
const mysql = require('../DB/db')
const goodTable = require('../services/goods')
const userTable = require('../services/user')
const $util = require('../util')




/* GET home page. */
router.get('/', async(req, res, next) => {
  let returnData = $util.get_check_data(['status'],req,res)
    if(returnData.pass){
      let page = Number(req.query.page) || 1
      let size = Number(req.query.size) || 10
      let status = req.query.status!=0?[Number(req.query.status)]:[1,2]
      let search = req.query.search?(req.query.search!='null'?req.query.search:''):''
      let Res = await goodTable.getGoodList(page,size,status,search)
      res.json({
        code:0,
        ...Res
      })
    }
});


router.get('/info', async(req, res, next) => {
  let returnData = $util.get_check_data(['id'],req,res)
  if(returnData.pass){
    let Res = await goodTable.getInfo(req.query.id)
    res.json({
      code:0,
      data:Res,
      msg:''
    })
  }
  
});

router.post('/updataInfo', async (req, res, next)=>{
  let returnData = $util.post_check_data(['id'],req,res)
  if(returnData.pass){
    let one = await goodTable.getInfo(req.body.id)
    if(one){
        let where = {
          updated:new Date()
        }
        if(req.body.title){
          where.title = req.body.title
        }
        if(req.body.price){
          where.price = req.body.price
        }
        if(req.body.sell_point){
          where.sell_point = req.body.sell_point
        }
        if(req.body.num){
          where.num = req.body.num
        }
        let Res = await goodTable.updataInfo(where,req.body.id)
        res.json({
          code:0,
          msg:`修改${Res?'成功':'失败'}`
        })

    }else{
      res.send({code:1,msg:'未找到商品'},500)
    }
  }
})

router.post('/updataOne', async(req,res,next)=>{
    let returnData = $util.post_check_data(['id','status'],req,res)
    if(returnData.pass){
      let one = await goodTable.getInfo(req.body.id)
      if(one){
        let where = {
          status:req.body.status,
          updated:new Date()
        }
        let Res = await goodTable.updataInfo(where,req.body.id)
        res.json({
          code:0,
          msg:`修改${Res?'成功':'失败'}`
        })
      }
    }
})

////////////////user///////////////////
router.post('/login',async(req,res,next)=>{
    let returnData = $util.post_check_data(['num','password'],req,res)
    if(returnData.pass){
        let info = await userTable.getUserInfo(req.body.num)
        if(!info){
          res.json({
            code:1,
            msg:'用户不存在'
          })
        }else{
          if(req.body.password == info.password){
            console.log('req.header',req.headers.authorization);
            res.json({
              code:0,
              token: await $util.CreatToken(req.body.id),
              msg:''
            })
          }else{
            res.json({
              code:1,
              msg:'密码不正确'
            })
          }
        }
    }
} )

router.post('/register',async(req,res,next)=>{
  let returnData = $util.post_check_data(['num','password','name'],req,res)
  if(returnData.pass){
      let info = await userTable.getUserInfo(req.body.num)
      if(info){
        res.json({
          code:1,
          msg:'用户已存在'
        })
      }else{
        let data = {
          name:req.body.name,
          u_num:req.body.num,
          password:req.body.password
        }
        let Res = await userTable.createdUser(data)
        if(Res){
          res.json({
            code:0,
            meg:'创建成功'
          })
        }else{
          res.send({code:1,msg:'创建失败'},500)
        }
      }
  }
} )

router.get('/userinfo', async(req, res, next) => {
  let returnData = $util.get_check_data(['num'],req,res)
  if(returnData.pass){
    let Res = await userTable.getUserInfo(req.query.num)
    res.json({
      code:0,
      data:Res,
      msg:''
    })
  }
  
});

module.exports = router;

var createError = require('http-errors');
var express = require('express');
// var path = require('path');
// var cookieParser = require('cookie-parser');
// var logger = require('morgan');

//middleware
const middle = require('./middleware')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const expressJwt = require('express-jwt')
const { tConfig } = require('./config/tokenConfig')
var app = express();

//websocket

const moment = require('moment')

const ws = require("nodejs-websocket");
console.log("开始建立连接...")


var groupmemberList = []//群成员
var oneofone = {}//一对一
// 向所有连接的客户端广播
function boardcast(obj) {
  // server.connections.forEach(function(conn) {
  //     conn.sendText(JSON.stringify(obj));
  // })

  if(obj.ofuid){
    oneofone[`${obj.ofuid}`].send(JSON.stringify(obj))
    // oneofone[`${obj.uid}`].send(JSON.stringify(obj))
    return;
  }else if(obj.groupID){
    groupmemberList.forEach((conn)=>{
      conn.send(JSON.stringify(obj))
    })
  }
}

function getDate(){
  return moment().format('YYYY-MM-DD HH:mm:ss')
}


var uList = [
  {uid:'1',uname:'test1'},
  {uid:'2',uname:'test2'},
  {uid:'3',uname:'test3'},
  {uid:'4',uname:'test4'},
  {uid:'5',uname:'test5'},
  {uid:'6',uname:'test6'},
]

var server = ws.createServer(function(conn){
  conn.on("text", function (str) {
    console.log("message:"+str)
    // conn.send(JSON.stringify({time:'2021-12-13',message:str}));
    let data = JSON.parse(str)
    console.log('data',data);
    oneofone[`${Number(data.uid)}`] = conn

    let returndata;
    //点击进入群聊界面
    if(data.groupID){
      //已经是群成员
      if(groupmemberList.length && groupmemberList.some(item=>{ return item == conn })){
        returndata = {
          textType:1,
          uid:data.uid,
          uname: uList.filter(item => { return item.uid == data.uid})[0].uname,
          time:getDate(),
          text:data.text,
          num:groupmemberList.length,
          groupID:data.groupID
        }
      }else{
        //新加入群聊
        groupmemberList.push(conn)
        returndata = {
          textType:0,
          uid:data.uid,
          uname: uList.filter(item => { return item.uid == data.uid})[0].uname,
          time:getDate(),
          text:'',
          num:groupmemberList.length,
          groupID:data.groupID
        }
      }
    }else if(data.ofuid){
      //私聊
      returndata = {
        textType:1,
        uid:data.uid,
        ofuid:data.ofuid,
        uname:uList.filter(item => { return item.uid == data.uid})[0].uname,
        ofuname:uList.filter(item => { return item.uid == data.ofuid})[0].uname,
        text:data.text,
        time:getDate()
      }
      conn.send(JSON.stringify(returndata))
    }else{
      returndata = {
        text:'连接成功'
      }
    }
    boardcast(returndata);
  })
  conn.on("close", function (code, reason) {
    console.log("关闭连接")
  });
  conn.on("error", function (code, reason) {
    console.log("异常关闭")
  });
}).listen(8001)



console.log("WebSocket建立完毕")

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));



// catch 404 and forward to error handler
// app.all('*',function(req, res, next) {
//   res.status = 404
//   next()
// });



app.use(expressJwt({
  secret: tConfig.key,  // 签名的密钥 或 PublicKey
  algorithms:['HS256'],
  credentialsRequired:true
}).unless({
  path: ['/index/login', '/index/register']  // 指定路径不经过 Token 解析
}))
app.use(function (err, req, res, next) { 
  if (err.name === 'UnauthorizedError') { 
  //  res.status(401).send('invalid token...');
   err.status = 401
   next(err)
    // err.status = 401;
    // err.message = 'invalid token...'
    // next(err)
    // res.send({})
  } 
});


app.use('/index',indexRouter);
app.use('/', usersRouter);
app.use(middle.not_found_middle);







// error handler
app.use(function(err, req, res, next) {
  console.log('err',err); 
  // set locals, only providing error in development 
  res.locals.message = err.message; 
  res.locals.error = req.app.get('env') === 'development' ? err : {}; 
  if(err.name === 'UnauthorizedError'){
    err.status = 401
    err.message = '401'
  }
  // render the error page 
  res.status(err.status || 500);
  
  // res.render('error'); 
  res.send(err.message,err.status)
}); 

 

module.exports = app;

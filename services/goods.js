// let { Goods } = require('../DB/model/index')


const _ = require('../DB/_')
const { Op } = require('sequelize');


async function getGoodList(page=1,size=10,status,search){
    let where = {
        offset: 0,
        limit: 10,
        where:{
            status:{
                [Op.or]:status
            },
            [Op.or]:[
                {
                    title:{
                        [Op.substring]:search
                    }
                },
                {
                    sell_point:{
                        [Op.substring]:search
                    }
                }
            ]
        }
    }
    where.offset = (page - 1)* size || where.offset
    where.limit = size || where.limit
    let data = await _.tb_item().findAndCountAll({...where})
    return data
}

async function getInfo(id){
    let data = await _.tb_item().findOne({
        where:{id:id}
    })
    return data
}

async function updataInfo(datas={},id){
    let data = await _.tb_item().update({...datas},{
        where:{ id:id }
    })
    if(data.length>0){
        return true
    }else{
        return false
    }
}


module.exports = {
    getGoodList,
    getInfo,
    updataInfo,
}